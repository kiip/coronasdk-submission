# KiipPlugin
Integrate kiip moment to award your users.
### Registration:
Please go to [Kiip Website](http://www.kiip.me/) to create an account and obtain a key and secret.
### Limitations:
It only works on iOS (device and ios simulator) and Android (device and android simulator). It does not work on Corona simulator.
### Project Settings:
If building for iOS, you must bypass App Transport Security (ATS) by adding the following to the plist table of build.settings.
```
settings =
{
iphone =
{
plist =
{
NSAppTransportSecurity = { NSAllowsArbitraryLoads=true },
},
},
}
```
Note: in Android the following permissions are added
```
"android.permission.INTERNET",
"android.permission.ACCESS_NETWORK_STATE",
"android.permission.ACCESS_COARSE_LOCATION",
"android.permission.ACCESS_FINE_LOCATION",
```
Note: in iOS the following frameworks are added
```
'CoreTelephony', 'QuartzCore', 'SystemConfiguration', 'AdSupport', 'Passkit', 'MediaPlayer'
```
### Functions:
place kiip init in your main.lua
``` lua
local kiip = require( "plugin.kiip" )
kiip.init(YOUR_KEY, YOUR_SECRET)
```
you can set testMode or extra properties
``` lua
kiip.setProperties({
testMode = true, --optional
email = "shane@gmail.com", --optional
gender = "male", --optional, either "male" or "female"
birthday = 1478059402000, -- optional, this is epoch time in milliseconds
capabilities = {"array 1", "array 2"}, --optional, array of string
shouldAutoRotate = false -- optional, ios only
})
```
to save a moment
``` lua
kiip.saveMoment({
saveMomentId = "test", -- required
value=20, -- optional 
onFailed = function(errMsg) -- optional, errMsg is String that tells the error message
end,
onFinished = function(hasPoptart) --optional, hasPoptart is a boolean tells if there is a poptart to display
if (hasPoptart) then
--if you have a poptart, you can call kiip.showPoptart({}) to display the poptart right away, or you can hold the poptart to display in later time
end
end
})
```
to display a poptart, note: prior to this call, you must call saveMoment to successfuly obtain a poptart.
``` lua
kiip.showPoptart({
onShowPoptart = function()--optional, callbacks when poptart displayed 
end,
onDismissPoptart = function()--optional, callbacks when poptart dismissed
end,
onShowNotification = function()--optional, callbacks when notification displayed
end,
onDismissNotification = function()--optional, callbacks when notification dismissed
end,
onClickNotification = function()--optional, callbacks when notification clicked
end,
onShowModal = function()--optional, callbacks when modal displayed
print("KiipPlugin, lua onShowModal")
end,
onDismissModal = function()--optional, callbacks when modal dismissed
print("KiipPlugin, lua onDismissModal")
end,
onVideoShow = function()--optional, callbacks when video played
print("KiipPlugin, lua onVideoShow")
end,
onVideoDismiss = function()--optional, callbacks when video dismissed
print("KiipPlugin, lua onVideoDismiss")
end
})
```
please notify kiip when the app is on foreground by calling startSession
``` lua
kiip.startSession({
onSessionStarted = function()--optional
end,
onSessionStartFailed = function(errMsg)--optional
end
})
```
please notify kiip when the app is in background by calling endSession
``` lua
kiip.endSession({
onSessionEnded = function()//optional
end,
onSessionEndFailed = function(errMsg)//optional
end
})
```
### Sample Code:
[click here](https://github.com/kiip/kiip-samples/tree/Feature/CoronaSamples/corona-samples/space-shooter) to see our sample project